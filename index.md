---
layout: default
title: Index
author: Liliana Millán Núñez
---


![](./images/egytp_logo.png)

Diciembre 2021

M. Sc. Liliana Millán Núñez liliana.millan@tec.mx


||Tema|
|:------|:------|
|1|[Introducción](0_introduccion.html)|
|2|[Datos](1_datos.html)|
|3|[Herramientas de análisis de datos](2_herramientas.html)|
